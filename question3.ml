(* ========================================================================== *)
(* =============================== QUESTION 3 =============================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


(* Fonction deduit_fait_nb_regles :
Permet de récupérer le numéro de règle sous forme de liste singleton si les
hypothèses sont satisfaites, et la liste vide sinon.

@param: regle une règle, fts une liste de faits
@return: le numéro de la règle en liste si elle est satisfaite, liste vide sinon
@type: 'a * prop * prop list -> prop list -> 'a list
@fail: si la liste devient inconsistante, la fonction échoue
*)
let deduit_fait_nb_regles regle fts =
    let n, csq, hyp = regle in
    let res =
        if (inclus hyp fts) then (cons csq fts)
        else fts in
    if check_consistency res then
        if (inclus hyp fts) then [n]
        else []
    else failwith "Liste de faits inconsistante";;


(* Fonction etape_nb_regles :
Permet de récupérer la liste des numéros de règles satisfaites.

@param: rgs une liste de règles, fts une liste de faits
@return: la liste des numéros de règles satisfaites
@type: ('a * prop * prop list) list -> prop list -> 'a list
*)
let rec etape_nb_regles rgs fts = match rgs with
    | [] -> []
    | [regle] -> deduit_fait_nb_regles regle fts
    | regle::q -> (deduit_fait_nb_regles regle fts)@(etape_nb_regles q (deduit_faits regle fts));;


(* Fonction itere_nb_regles :
Permet de récupérer la liste des numéros de règles amenant au but, c'est à dire
la preuve du but.

@param: but le fait recherché, rgs une liste de règles, fts une liste de faits
@return: les numéros des règles utilisées pour atteindre le but, ou échec sinon
@type: prop -> ('a * prop * prop list) list -> prop list -> 'a list
@fail: si le but n'est pas atteint, la fonction échoue
*)
let itere_nb_regles but rgs fts =
    let rec aux but rgs rgs_nb fts fts_old =
        let fts_new = (etape rgs fts) in
        let rgs_new = (concat (etape_nb_regles rgs fts) rgs_nb) in
        if (fts_new = fts_old) || (appartient but fts_new) then fts_new, rgs_new
        else (aux but rgs rgs_new fts_new fts)
    in let fts_new, rg_nb = (aux but rgs [] fts []) in
    if appartient but fts_new then rg_nb else failwith "But non atteint";;


(* Fonction liste_faits_to_string_bis :
Convertit une liste de faits en la chaîne de caractères associée
"Fait1 et Fait2 et ... ET FaitN", en mettant en majuscule les faits présents
dans la liste de faits initiaux

@param: fts une liste de faits, deb une liste de faits initiaux
@return: la chaîne de caractères associée à la liste de faits
@type: prop list -> prop list -> string
*)
let rec liste_faits_to_string_bis fts deb = match fts with
    | [] -> ""
    | [a] ->
        if (appartient a deb) then String.uppercase(fait_to_string a)
        else fait_to_string a
    | t::q ->
        if (appartient t deb) then String.uppercase(fait_to_string t) ^ " et " ^ (liste_faits_to_string_bis q deb)
        else (fait_to_string t) ^ " et " ^ (liste_faits_to_string_bis q deb);;


(* Fonction regle_to_string_bis :
Convertit une règle en la chaîne de caractères
associée "FAIT et FAIT et ... donnent CSQ par la regle numéro ..."

@param: rg une règle, deb une liste de faits initiaux, but le but
@return: la chaîne de caractères associée à la règle
@type: int * prop * prop list -> prop list -> prop -> string
*)
let regle_to_string_bis rg deb but =
    let (n,conseq,hyp) = rg in
    let fct_concat = (liste_faits_to_string_bis hyp deb) in
    let csq_string = if (but = conseq) then String.uppercase(fait_to_string conseq) else (fait_to_string conseq) in
    if (but = conseq) then fct_concat ^ " donnent " ^ csq_string ^ " (CQFD) par la regle " ^ (string_of_int n)
    else fct_concat ^ " donnent " ^ csq_string ^ " par la regle " ^ (string_of_int n);;


(* Fonction preuve_to_string :
Convertit une preuve en une chaînes de caractères (donc chaque règle intercalées
par des sauts de lignes)

@param: rgs une liste de règles, fts une liste de faits, but le but recherché
@return: la chaîne de caractères associée à la preuve
@type: (int * prop * prop list) list -> prop list -> prop -> string
*)
let preuve_to_string rgs fts but =
    let rg_nb = (itere_nb_regles but rgs fts) in
    let rec aux rgs fts but rg_nb = match rgs with
        | [] -> ""
        | (n,csq,hyp)::r ->
            if (appartient n rg_nb) then (regle_to_string_bis (n,csq,hyp) fts but) ^ ".\n" ^ (aux r fts but rg_nb)
            else (aux r fts but rg_nb)
    in (aux rgs fts but rg_nb);;


(* Fonction affiche_preuve :
Affiche dans la console la chaîne de caractère associée à la preuve.

@param: rgs une liste de règles, fts une liste de faits, but le but recherché
@return: affiche la preuve
@type: (int * prop * prop list) list -> prop list -> prop -> unit
*)
let affiche_preuve rgs fts but = Pervasives.print_string (preuve_to_string rgs fts but);;
