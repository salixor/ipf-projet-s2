(* ========================================================================== *)
(* =============================== QUESTION 2 =============================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


(* Fonction fait_to_string :
Convertit un fait en la chaîne de caractères associée.

@param: ft un fait
@return: la chaîne de caractères associée au fait
@type: prop -> string
*)
let fait_to_string ft = match ft with
    | Var(a) -> a
    | Non(a) -> "NON " ^ a;;


(* Fonction liste_faits_to_string :
Convertit une liste de faits en la chaîne de caractères
associée "Fait1 ET Fait2 ET ... ET FaitN"

@param: fts une liste de faits
@return: la chaîne de caractères associée à la liste de faits
@type: prop list -> string
*)
let rec liste_faits_to_string fts = match fts with
    | [] -> ""
    | [a] -> fait_to_string a
    | t::q -> (fait_to_string t) ^ " ET " ^ (liste_faits_to_string q);;


(* Fonction regle_to_string :
Convertit une règle en la chaîne de caractères associée "SI ... ALORS ..."

@param: rg une règle
@return: la chaîne de caractères associée à la règle
@type: 'a * prop * prop list -> string
*)
let regle_to_string rg =
    let (_,csq,hyp) = rg in
    "SI " ^ (liste_faits_to_string hyp) ^ " ALORS " ^ (fait_to_string csq);;


(* Fonction affiche_regles :
Affiche dans la console les chaînes de caractères associées aux règles de la
liste de règles, en sautant une ligne pour chaque règle.

@param: rgs une base de règles
@return: affiche les règles contenues dans la liste de règles
@type: ('a * prop * prop list) list -> unit
*)
let rec affiche_regles rgs = match rgs with
    | [] -> Pervasives.print_string ""
    | rg::q -> Pervasives.print_string ((regle_to_string rg) ^ "\n"); (affiche_regles q);;
