(* ========================================================================== *)
(* =================== DÉFINISSION DES VARIABLES ET TYPES =================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


    (* Type des propriétés *)
type prop = Var of string | Non of string;;

    (* DOMAINE : animaux *)
let rg_1_animaux = (1, Var "chien", [Var "taille moyenne"; Var "aboiement"]);;
let rg_2_animaux = (2, Var "chat", [Var "taille moyenne"; Var "miaulement"]);;
let rg_3_animaux = (3, Var "souris", [Var "taille petite"; Var "chicotement"]);;

    (* DOMAINE : botanique *)
let rg_1_botanique = (1, Var "phanerogame", [Var "fleur"; Var "graine"]);;
let rg_2_botanique = (2, Var "sapin", [Var "phanerogame"; Var "graine nue"]);;
let rg_3_botanique = (3, Var "monocotyledone", [Var "phanerogame"; Var "1-cotyledone"]);;
let rg_4_botanique = (4, Var "dicotyledone", [Var "phanerogame"; Var "2-cotyledone"]);;
let rg_5_botanique = (5, Var "muguet", [Var "monocotyledone"; Var "rhizome"]);;
let rg_6_botanique = (6, Var "anémone", [Var "dicotyledone"]);;
let rg_7_botanique = (7, Var "lilas", [Var "monocotyledone"; Non "rhizome"]);;
let rg_8_botanique = (8, Var "cryptogame", [Var "feuilles"; Var "fleur"]);;
let rg_9_botanique = (9, Var "mousse", [Var "cryptogame"; Non "racine"]);;
let rg_10_botanique = (10, Var "fougere", [Var "cryptogame"; Var "racine"]);;
let rg_11_botanique = (11, Var "algue", [Var "thallophyte"; Var "chlorophylle"]);;
let rg_12_botanique = (12, Var "champignon", [Var "thallophyte"; Non "chlorophylle"]);;
let rg_13_botanique = (13, Var "thallophyte", [Non "feuilles"; Var "plante"]);;
let rg_14_botanique = (14, Var "champignon", [Var "thallophyte"; Non "chlorophylle"]);;
let rg_15_botanique = (15, Var "colibacille", [Non "feuilles"; Non "fleur"; Non "plante"]);;

    (* DOMAINE : animaux *)
let rg_1_animaux = (1, Var "mammifere", [Var "animal a poils"]);;
let rg_2_animaux = (2, Var "mammifere", [Var "donne lait"]);;
let rg_3_animaux = (3, Var "carnivore", [Var "mange viande"]);;
let rg_4_animaux = (4, Var "carnivore", [Var "animal a dents pointues"; Var "animal a griffes"; Var "animal a yeux vers avant"]);;
let rg_5_animaux = (5, Var "ongule", [Var "mammifere"; Var "animal a sabots"]);;
let rg_6_animaux = (6, Var "ongule", [Var "mammifere"; Non "carnivore"]);;
let rg_7_animaux = (7, Var "guépard", [Var "mammifere"; Var "carnivore"; Var "couleur brune"; Var "tâches sombres"]);;
let rg_8_animaux = (8, Var "tigre", [Var "mammifere"; Var "carnivore"; Var "couleur brune"; Var "raies noires"]);;
let rg_9_animaux = (9, Var "girafe", [Var "ongule"; Var "long cou"; Var "longues pattes"; Var "tâches sombres"]);;
let rg_10_animaux = (10, Var "zebre", [Var "ongule"; Var "raies noires"]);;
let rg_11_animaux = (11, Var "oiseau", [Var "plumes"]);;
let rg_12_animaux = (12, Var "oiseau", [Var "vole"; Var "ponds oeufs"]);;
let rg_13_animaux = (13, Var "autruche", [Var "oiseau"; Non "vole"; Var "long cou"; Var "longues pattes"; Var "noir et blanc"]);;
let rg_14_animaux = (14, Var "pingouin", [Var "oiseau"; Non "vole"; Var "nage"; Var "noir et blanc"]);;
let rg_15_animaux = (15, Var "aigle", [Var "oiseau"; Var "carnivore"]);;
