(* ========================================================================== *)
(* ================================= TESTS ================================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


let regles_tests_botanique = [ rg_1_botanique ; rg_5_botanique ; rg_3_botanique ];;
let faits_tests_botanique = [ Var "rhizome" ; Var "fleur" ; Var "graine" ; Var "1-cotyledone" ];;

deduit_faits rg_1_botanique faits_tests_botanique;;
deduit_faits rg_3_botanique faits_tests_botanique;;
deduit_faits rg_5_botanique faits_tests_botanique;;
etape regles_tests_botanique faits_tests_botanique;;
itere (Var "muguet") regles_tests_botanique faits_tests_botanique;;

affiche_regles regles_tests_botanique;;

deduit_fait_nb_regles rg_1_botanique faits_tests_botanique;;
deduit_fait_nb_regles rg_3_botanique faits_tests_botanique;;
deduit_fait_nb_regles rg_5_botanique faits_tests_botanique;;
etape_nb_regles regles_tests_botanique faits_tests_botanique;;
itere_nb_regles (Var "muguet") regles_tests_botanique faits_tests_botanique;;
itere_nb_regles (Var "algue") regles_tests_botanique faits_tests_botanique;;

affiche_preuve regles_tests_botanique faits_tests_botanique (Var "muguet");;
affiche_preuve regles_tests_botanique faits_tests_botanique (Var "algue");;



let regles_tests_botanique_bis = [ rg_1_botanique ; rg_5_botanique ; rg_3_botanique ; rg_4_botanique ; rg_8_botanique ];;
let faits_tests_botanique_bis = [ Var "rhizome" ; Var "fleur" ; Var "graine" ; Var "1-cotyledone" ; Var "2-cotyledone" ];;

itere (Var "muguet") regles_tests_botanique_bis faits_tests_botanique_bis;;
itere (Var "dicotyledone") regles_tests_botanique_bis faits_tests_botanique_bis;;
itere (Var "champignon") regles_tests_botanique_bis faits_tests_botanique_bis;;

itere_nb_regles (Var "muguet") regles_tests_botanique_bis faits_tests_botanique_bis;;
itere_nb_regles (Var "dicotyledone") regles_tests_botanique_bis faits_tests_botanique_bis;;
itere_nb_regles (Var "champignon") regles_tests_botanique_bis faits_tests_botanique_bis;;

affiche_preuve regles_tests_botanique_bis faits_tests_botanique_bis (Var "muguet");;
affiche_preuve regles_tests_botanique_bis faits_tests_botanique_bis (Var "dicotyledone");;
affiche_preuve regles_tests_botanique_bis faits_tests_botanique_bis (Var "champignon");;
