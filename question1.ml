(* ========================================================================== *)
(* =============================== QUESTION 1 =============================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


(* Fonction est_negation :
Indique si un fait est une négation.

@param: ft un fait
@return: true si ft est une négation, false sinon
@type: prop -> bool
*)
let est_negation ft = match ft with
    | Var _ -> false
    | Non _ -> true;;


(* Fonction check_consistency :
Vérifie si une liste de faits est consistante, c'est à dire ne contient pas
un fait et sa négation.

@param: fts une liste de faits
@return: true si la liste de faits est consistante, false sinon
@type: prop list -> bool
*)
let check_consistency fts =
    let l1, l2 = partition est_negation fts in
    let rec aux l = match l with
        | [] -> true
        | (Var _)::q -> true
        | (Non a)::q -> (not (appartient (Var a) l2)) && (aux q) in
    aux l1;;


(* Fonction deduit_faits :
Applique une règle à la liste de faits et permet de déduire la nouvelle liste.

@param: regle une règle, fts une liste de faits
@return: la concaténation de la conséquence de la règle et de la liste de faits
@type: 'a * prop * prop list -> prop list -> prop list
@fail: si la liste devient inconsistante, la fonction échoue
*)
let deduit_faits regle fts =
    let _, csq, hyp = regle in
    let res =
        if (inclus hyp fts) then (cons csq fts)
        else fts in
    if check_consistency res then res else failwith "Liste de faits inconsistante";;


(* Fonction etape :
Applique chaque règle de la base de règles une fois à la liste de faits pour
déduire une nouvelle liste de faits.

@param: rgs une liste de règles, fts une liste de faits
@return: la liste de faits déduite en appliquant une fois les règles de rgs à fts
@type: ('a * prop * prop list) list -> prop list -> prop list
*)
let rec etape rgs fts = match rgs with
    | [] -> fts
    | regle::q -> etape q (deduit_faits regle fts);;


(* Fonction itere :
Itère la fonction etape tant que le but n'est pas atteint ou qu'un nouveau
fait est déduit.

@param: but le fait recherché, rgs une liste de règles, fts une liste de faits
@return: la liste de faits finale
@type: prop -> ('a * prop * prop list) list -> prop list -> prop list
*)
let itere but rgs fts =
    let rec aux but rgs fts fts_old =
        let fts_new = (etape rgs fts) in
        if (fts_new = fts_old) || (appartient but fts_new) then fts_new
        else (aux but rgs fts_new fts)
    in aux but rgs fts [];;
