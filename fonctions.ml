(* ========================================================================== *)
(* ======================== FONCTIONS INTERMÉDIAIRES ======================== *)
(* ========================================================================== *)
(*                     Auteur : Kévin Cocchi, Groupe 3.2                      *)


(* Fonction appartient :
Teste l'appartenance d'un élément à une liste.

@param: x un élément, l une liste
@return: true si x appartient à l, false sinon
@type: 'a -> 'a list -> bool
*)
let rec appartient x l = match l with
    | [] -> false
    | t::q -> (t = x) || (appartient x q);;


(* Fonction inclus :
Teste l'inclusion d'une liste dans une autre.

@param: l1 et l2 deux listes
@return: true si l1 est incluse dans l2, false sinon
@type: 'a list -> 'a list -> bool

@note: permet de vérifier que les hypothèses sont dans la base de faits
*)
let rec inclus l1 l2 = match l1, l2 with
    | [], _ -> true
    | _, [] -> false
    | t::q, l2 -> (appartient t l2) && (inclus q l2);;


(* Fonction cons :
Concaténation d'un élément à une liste sans créer de doublons.

@param: x un élément, l une liste
@return: la concaténation de x et l sans doublon
@type: 'a -> 'a list -> 'a list

@note: permet d'ajouter un fait à une liste de faits, sans créeer de doublons
*)
let cons x l = if appartient x l then l else x::l;;


(* Fonction concat :
Concaténation de deux listes (sans doublons).

@param: l1 et l2 deux listes
@return: une liste contenant les éléments de l1 et l2, sans doublons
@type: 'a list -> 'a list -> 'a list
*)
let rec concat l1 l2 = match l1, l2 with
    | _, [] -> l1
    | [], _ -> l2
    | t::q, _ -> if (appartient t l2) then (concat q l2) else t::(concat q l2);;


(* Fonction partition :
Séparation d'une liste en deux listes, l'une constituée des éléments
vérifiant le prédicat, l'autre de ceux ne le vérifiant pas.

@param: p un prédicat, l une liste
@return: deux listes l1 et l2, l1 contenant les éléments satisfaisant p
@type: ('a -> bool) -> 'a list -> 'a list * 'a list

@note: permet de vérifier qu'une liste de faits reste consistante
*)
let partition p l =
    let rec aux l1 l2 l = match l with
        | [] -> l1, l2
        | t::q -> if (p t) then aux (t::l1) l2 q else aux l1 (t::l2) q
    in aux [] [] l;;
